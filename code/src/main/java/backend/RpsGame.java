//Rayane Knadry
package backend;

import java.util.*;

public class RpsGame {
    int wins;
    int ties;
    int losses;
    Random randomizer = new Random();

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String choice) {
        int computerChoice = randomizer.nextInt(3); // 0 for rock, 1 for scissors, 2 for paper
        String computerSymbol = getSymbol(computerChoice);

        System.out.println("Computer plays " + computerSymbol);

        int playerChoice = getChoiceNumber(choice);

        // Determine the winner
        if ((playerChoice + 1) % 3 == computerChoice) {
            // Player wins
            wins++;
            System.out.println("You won");
            return "You won";

        } else if (playerChoice == computerChoice) {
            // It's a tie
            ties++;
             System.out.println("It's a tie!");
             return "It's a tie!";
        } else {
            // Computer wins
            losses++;
            System.out.println("Computer won!");
             return "Computer won!";
        }
    }

    private int getChoiceNumber(String choice) {
        switch (choice.toLowerCase()) {
            case "rock":
                return 0;
            case "scissors":
                return 1;
            case "paper":
                return 2;
            default:
                throw new IllegalArgumentException("Invalid choice. Choose rock, paper, or scissors.");
        }
    }

    // Helper method to get the symbol based on the choice number
    private String getSymbol(int choice) {
        switch (choice) {
            case 0:
                return "rock";
            case 1:
                return "scissors";
            case 2:
                return "paper";
            default:
                throw new IllegalArgumentException("Invalid choice number.");
        }
    }
}
