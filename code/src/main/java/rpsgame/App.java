//Rayane Knadry
package rpsgame;
import java.util.*;

import backend.RpsGame;
/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //TODO
        while (true){
            Scanner scan=new Scanner(System.in);
            String choice=scan.nextLine();
            RpsGame game=new RpsGame();
            game.playRound(choice);
        }
    }
}
