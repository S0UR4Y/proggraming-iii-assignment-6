//Rayane Knadry
package frontend;

import backend.RpsGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent> {

    // fields
    private String playerChoice;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private TextField message;
    private RpsGame game;

    public RpsChoice(String playerChoice, TextField wins, TextField losses, TextField ties, TextField message,
            RpsGame game) {
        this.playerChoice = playerChoice;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.message = message;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e) {
        // Get the result of the round from the playRound() method
        String roundResult = game.playRound(playerChoice); 
    
        // Update the message TextField with the round result
        message.setText("Round Result: " + roundResult);
    
        // Now, update the wins, losses, and ties TextFields as per your requirements
        int winCount = game.getWins();
        int lossCount = game.getLosses();
        int tieCount = game.getTies();
    
        wins.setText("Wins: " + winCount);
        losses.setText("Losses: " + lossCount);
        ties.setText("Ties: " + tieCount);
    }
    

}
